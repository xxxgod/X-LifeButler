# X-LifeButler

#### 介绍
个人事务后台管理系统，记账、规划、目标

#### 软件架构
-  SpringBoot
-  Mybatis
-  Shiro
-  Thymeleaf
-  Bootstrap

#### 使用说明

1. 登录页面

![输入图片说明](doc/%E7%99%BB%E5%BD%95%E9%A1%B5%E9%9D%A2.jpg)

2. 后台首页

![输入图片说明](doc/%E5%90%8E%E5%8F%B0%E9%A6%96%E9%A1%B5.jpg)

3. 收支管理
![输入图片说明](doc/%E6%94%B6%E6%94%AF%E7%AE%A1%E7%90%86.jpg)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

